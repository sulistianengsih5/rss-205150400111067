package com.example.rss_205150400111067.model;

public class Feed {

    public String url;
    public String title;
    public String link;
    public String author;
    public String desc;
    public String img;

    public Feed(String url, String title, String link, String author, String desc, String img) {
        this.url = url;
        this.title = title;
        this.link = link;
        this.author = author;
        this.desc = desc;
        this.img = img;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getImage() {
        return img;
    }

    public void setImage(String image) {
        this.img = image;
    }
}
